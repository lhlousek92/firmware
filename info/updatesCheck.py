#!/usr/bin/python3
"""updatesCheck.py: program pro zjisteni nove verze"""
__author__      = "Ladislav Hlousek"
__copyright__   = "Copyright 2018, Ladislav Hlousek"
__version__     = "1.0.0"
__maintainer__  = "Ladislav Hlousek"
__email__       = "ladishlousek@gmail.com"
__status__      = "Production"

import os,sys,json

def main():
	try:
		os.system("sudo sh /home/pi/install.sh 11")
		data = ""
		with open("/home/pi/info/versions.txt") as file:
			data = file.readlines()
			file.close()
		i = 0
		for line in data:
			line = line.rstrip("\n")
			line = line[-5:]
			data[i] = line
			i = i + 1
		lastIndexVersion = len(data)-1
		newVersion = data[lastIndexVersion]
		foundedNew = False
		
		with open("/home/pi/info/info.json") as f:
			jsonString = f.read()
			fileData = json.loads(jsonString)			
			if newVersion != fileData["Version"]:
				foundedNew = True
			else:
				foundedNew = False
			f.close()
		
		#print(newVersion,foundedNew,fileData)
		
		fileData["NeedUpdate"] = foundedNew
		fileData["NewVersion"] = newVersion
				
		updated = os.path.exists("/home/pi/info/updated")
		if updated == True:
			fileData["Version"] = newVersion
			os.remove("/home/pi/info/updated")
			
		jsonOut = json.dumps(fileData)
		
		with open("/home/pi/info/info.json", "w") as outfile:
			outfile.write(jsonOut)
			outfile.close()
		
	except Exception as e:
		print("Error: " + str(e))
		pass

if __name__ == '__main__':
	main()