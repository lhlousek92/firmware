#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>
#include <sqlite3.h>

#define VENTIL_PIN 0

struct ActualData {
	int pocetLitry;
	int pocetLitryCelkem;
	int stavVentil;
	int autoMod;
	int forceMod;
};

void sendMail()
{
	char cmd[1024] = "";	
	sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"Ventil otevřen\"");
	system(cmd);
}

struct ActualData getActualData()
{
	struct ActualData actData = { 0, 0, 0, 0 , 0};
		
	const char *sqlVodData = "SELECT PocetLitry, CelkemLitry FROM vodomer_data ORDER BY TsTime DESC LIMIT 1";
	const char *sqlVenData = "SELECT StavVentil, AutoAkce, Force FROM ventil_data ORDER BY TsTime DESC LIMIT 1";
	
	sqlite3_stmt *resVod, *resVen;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
		
	int rc = sqlite3_prepare_v2(db, sqlVodData, -1, &resVod, 0);
	int rc1 = sqlite3_prepare_v2(db, sqlVenData, -1, &resVen, 0);
	
	if (rc != SQLITE_OK && rc1 != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_step(resVod);
	rc1 = sqlite3_step(resVen);
	
	if (rc == SQLITE_ROW) {
		actData.pocetLitry = (int)sqlite3_column_int(resVod, 0);
		actData.pocetLitryCelkem = (int)sqlite3_column_int(resVod, 1);
	} else {
		delay(50);
		rc = sqlite3_step(resVod);
		if (rc == SQLITE_ROW) {
			actData.pocetLitry = (int)sqlite3_column_int(resVod, 0);
			actData.pocetLitryCelkem = (int)sqlite3_column_int(resVod, 1);
		}
	}
	
	if (rc1 == SQLITE_ROW) {
		actData.stavVentil = (int)sqlite3_column_int(resVen, 0);
		actData.autoMod = (int)sqlite3_column_int(resVen, 1);
		actData.forceMod = (int)sqlite3_column_int(resVen, 2);
	} else {
		actData.stavVentil = -1;
		actData.autoMod = 1;
		actData.forceMod = 0;
		delay(50);
		rc1 = sqlite3_step(resVen);
		if (rc1 == SQLITE_ROW) {
			actData.stavVentil = (int)sqlite3_column_int(resVen, 0);
			actData.autoMod = (int)sqlite3_column_int(resVen, 1);
			actData.forceMod = (int)sqlite3_column_int(resVen, 2);
		}
	}
	
	sqlite3_finalize(resVod);
	sqlite3_finalize(resVen);
	sqlite3_close(db);
	return actData;
}

unsigned int getStavVentilDB()
{
	unsigned int stav = 2;
	
	const char *sqlVenData = "SELECT StavVentil FROM ventil_data ORDER BY TsTime DESC LIMIT 1";
	
	sqlite3_stmt *res;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sqlVenData, -1, &res, 0);
	rc = sqlite3_step(res);
	if (rc == SQLITE_ROW) {
		stav = (int)sqlite3_column_int(res, 0);
	} else {
		delay(50);
		rc = sqlite3_step(res);
		if (rc == SQLITE_ROW)
			stav = (int)sqlite3_column_int(res, 0);
	}
	
	sqlite3_finalize(res);
	sqlite3_close(db);
	return stav;
}


int insertVodData(int pocetL, int celkemL, double prutok, int stavVentil)
{
	int id = 0;
	
	const char *sql = "INSERT INTO vodomer_data (PocetLitry, Prutok, StavVentil, CelkemLitry) VALUES (?,?,?,?)";
	sqlite3_stmt *res;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
		sqlite3_bind_int(res,1,pocetL);
		sqlite3_bind_double(res,2,prutok);
		sqlite3_bind_int(res,3,stavVentil);
		sqlite3_bind_int(res,4,celkemL);
	
		rc = sqlite3_step(res);
    
		if (rc != SQLITE_DONE) { 
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE) 
				printf("execution failed: %s\n", sqlite3_errmsg(db));
		}
		
		id = sqlite3_last_insert_rowid(db);
		
		sqlite3_finalize(res);
		sqlite3_close(db);
	}
	return id;
}

int main (void)
{
  unsigned int dbLastStavVentil;
  unsigned int dbStavVentil;
  struct ActualData actData = {0,0,0,0,0};
  int pocetLitryCelkem = 0;
  int ioStav = 0;
  wiringPiSetup();
  pinMode(VENTIL_PIN, OUTPUT);
  
  dbStavVentil = getStavVentilDB();
  if(dbStavVentil < 2)
  {
	  digitalWrite(VENTIL_PIN, dbStavVentil);
  } else {
	  dbStavVentil = digitalRead(VENTIL_PIN);
  }
  ioStav = dbStavVentil;
  dbLastStavVentil = dbStavVentil;
  
  
  digitalWrite(VENTIL_PIN, dbStavVentil);
  
  while(1)
  {
	dbStavVentil = getStavVentilDB();
	ioStav = digitalRead(VENTIL_PIN);
	
	if(dbStavVentil != 2 && dbStavVentil != dbLastStavVentil && dbStavVentil != ioStav)
	{
		digitalWrite(VENTIL_PIN, dbStavVentil);
		
		dbLastStavVentil = dbStavVentil;
		
		if(dbStavVentil == 1)
		{
			actData = getActualData();
			if(actData.pocetLitryCelkem > 0)
				pocetLitryCelkem = actData.pocetLitryCelkem;
		
			insertVodData(0, pocetLitryCelkem, 0, dbStavVentil);
			sendMail();
		}
		
		printf("Ventil %d \n", dbStavVentil);
		delay(100);
	} else {
		delay(1000);
	}
  }
}
