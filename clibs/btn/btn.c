#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>
#include <time.h>
#include <sqlite3.h>

#define VENTIL_PIN 0
#define VENTIL_BTN_PIN 7

unsigned int btnMod;
void ventilBtnDownISR (void);
unsigned int ioStav;

void setStavVentilDB(unsigned int stav)
{
	const char *sql = "INSERT INTO ventil_data (StavVentil, AutoAkce, Force) VALUES (?,?,?)";
	sqlite3_stmt *res;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
		
	if (rcc != SQLITE_OK)
	{
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
	}
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {	
		sqlite3_bind_int(res,1,stav);
		sqlite3_bind_int(res, 2, 2); //0-web, 1-automat, 2-už. tlačítko
		sqlite3_bind_int(res,3,1);
		rc = sqlite3_step(res);
		
		if (rc != SQLITE_DONE)
		{
			delay(50);
			rc = sqlite3_step(res);
			
			if (rc != SQLITE_DONE)
			{
				printf("execution failed: %s", sqlite3_errmsg(db));
			}			
		} 
		
		sqlite3_finalize(res);
		sqlite3_close(db);		
	}
}


void doReset()
{
	char cmd[1024] = "";
	
	if(ioStav == 1)
	{
		digitalWrite(VENTIL_PIN,0);
		delay(500);
	}
	digitalWrite(VENTIL_PIN,1);
	delay(500);
	digitalWrite(VENTIL_PIN,0);
	delay(500);
	digitalWrite(VENTIL_PIN,1);
	delay(500);
	digitalWrite(VENTIL_PIN,0);
	delay(250);
	sprintf(cmd, "sudo sh /home/pi/install.sh 9");
	system(cmd);
}

int main (void)
{
  ioStav = 0;
  
  btnMod = 0;
  
  wiringPiSetup();  
  pinMode(VENTIL_BTN_PIN, INPUT);
  pinMode(VENTIL_PIN, OUTPUT);
    
  pullUpDnControl(VENTIL_BTN_PIN, PUD_DOWN);
  
  if (wiringPiISR(VENTIL_BTN_PIN, INT_EDGE_FALLING, &ventilBtnDownISR) < 0)
  {
	  printf("Unable to setup ISR (ventilBtnDownISR) \n");
	  return 1;
  }
  
  
  ioStav = digitalRead(VENTIL_PIN);
  int btnLevel = digitalRead(VENTIL_BTN_PIN);
  while(1)
  {	
    if(btnMod == 1)
    {
		ioStav = digitalRead(VENTIL_PIN);
		//digitalWrite(VENTIL_PIN, !ioStav);
		setStavVentilDB(!ioStav);
		printf("Ventil %d \n", !ioStav);
		
		delay(1000);
		btnLevel = digitalRead(VENTIL_BTN_PIN);
		if(btnLevel == 0)
		{
			printf("still down\n");
			delay(5000);
			btnLevel = digitalRead(VENTIL_BTN_PIN);
			if(btnLevel == 0)
			{
				doReset();
			}
		}
		btnMod = 0;
	} 
	else 
	{
		delay(50);
	}
  }
}

void ventilBtnDownISR (void)
{
	if(btnMod == 0)
		btnMod = 1;
}
