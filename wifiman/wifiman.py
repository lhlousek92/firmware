#!/usr/bin/python3

import os
import time
import urllib
import base64
from flask import Flask, render_template, request, json, jsonify
import wifi
import sys
sys.path.append('.')
sys.path.append('..')
sys.path.append('static')


def Search():
  wifilist = []
  res = []
  cells = wifi.Cell.all('wlan0')
  for cell in cells:
    wifilist.append(cell)

  for wp in wifilist:
    e = ""
    if wp.encrypted:
      e = "encrypted"
    obj = {
      'ssid': wp.ssid,
      'encrypted': e
    }
    res.append(obj)

  return res


def writeNetwork(ssid,password=""):
  data = ""
  if password != "":
    data = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=CZ\nnetwork={\n    ssid=\""+str(ssid)+"\"\n    psk=\""+str(password, 'utf-8')+"\"\n    key_mgmt=WPA-PSK\n}"
  else:
    data = "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\nupdate_config=1\ncountry=CZ\nnetwork={\n    ssid=\""+str(ssid)+"\"\n    psk=\"\"\n}"

  with open("/etc/wpa_supplicant/wpa_supplicant.conf", "w+") as wpa:
    wpa.write(data)


def create_app():
    app = Flask(__name__, static_folder='static')

    @app.route("/")
    def index():
        points = Search()
        for point in points:
            point['ssid_encoded'] = urllib.parse.quote(point['ssid'])
        return render_template("index.html", points=points)


    @app.route("/confirm")
    def confirm():
        ssid = request.args.get("ssid", "")
        ssid_encoded = urllib.parse.quote(ssid.encode())
        encrypted = request.args.get("encrypted", "unencrypted")

        return render_template(
                                "confirm.html",
                                ssid=ssid,
                                encrypted=encrypted,
                                ssid_encoded=ssid_encoded,
                                mode=mode,
                                )


    @app.route('/confirmed', methods=['POST'])
    def confirmed():
        os.system("sudo wpa_cli -i wlan0 reconfigure && sudo systemctl restart wpa_supplicant@wlan0.service")
        #os.system("sudo reboot")
        jsonify("ok")


    @app.route("/connect", methods=['POST'])
    def connect():
        ssid = urllib.parse.unquote(request.form["ssid"])
        password = request.form["password"].encode()
        writeNetwork(ssid,password)
        return render_template("connect.html",
                                ssid=ssid,
                                password=password,
                              )

    return app


def main():
    app = create_app()
    app.run(host="0.0.0.0", port=81, debug=False, threaded=True)


if __name__ == '__main__':
    main()
