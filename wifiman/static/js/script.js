$(document).ready(function() {
	
	/*
	$("#btn-wifi-save").click(function(){
		var ssid = $("input[name='ssid']").val();
		if(ssid == "") {
			popup.open("button", "Chyba", "Není vyplněný název síťě!");
			return false;
		}
		pageWifi.ssid_new = ssid;
		var password = $("input[name='password']").val();

		$("input[name='ssid']").val("");
		$("input[name='password']").val("");

		popup.open("spinner", "Ukládání WiFi");
		var postData = "{\"ssid\":\""+ssid+"\",\"password\":\""+password+"\"}";
		request.post("pageWifi", "wifiPostCallback", "/api/wifi", postData);
	});*/
	
	$(".btn-connect").click(function(){
		$("input[name='ssid']").val($(this).val());
		$("input[name='password']").focus();
	});
	
	$('#wifiForm').submit(function() {		
		var ssid = $("input[name='ssid']").val();
		if(ssid == "") {
			popup.open("button", "Chyba", "Není zvolen název síťě!");
			return false;
		}

		return true;
    //return true; // return false to cancel form action
	});
	

	popup.init();
	
});



var popup = {

	init: function(){

		var el = ''+
		  '<div id="popup1" class="popup-overlay">'+
		    '<div class="popup">'+
		      '<h3 id="popup1-title"></h3>'+
		      '<div class="content" id="popup1-content">'+
		      '</div>'+
		      '<div class="form-group m-t-50">'+
		        '<button type="submit" class="react-boostrap-button-wrapper btn btn-primary btn-short" id="btn-popup1-ok">'+
		          '<div class="">OK</div>'+
		        '</button>'+
		        '<div class="img-spinner m-y-20" id="popup1-spinner"></div>'+
		      '</div>'+
		    '</div>'+
		  '</div>';

		$('#app-loading').append(el);		
		// zavírání pop-up okna
  	$("#btn-popup1-ok").click(function(){
			popup.close();
  	});
	},

	// spinner, button
  open: function(type, title, content="") {
  	$('#popup1-title').html(title);
  	$('#popup1-content').html(content);
  	$('#btn-popup1-ok').hide();
  	$('#popup1-spinner').hide();
  	if(type == "button") $('#btn-popup1-ok').show();
  	else if(type == "spinner") $('#popup1-spinner').show();
  	$(".popup-overlay").css( "visibility", "visible" );
		$(".popup-overlay").css( "opacity", "1" );
  },

  close: function() {
  	$(".popup-overlay").css( "opacity", "0" );
		$(".popup-overlay").css( "visibility", "hidden" );
  },
}

