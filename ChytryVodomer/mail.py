import os,sys
import sqlite3
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import sleep

print(len(sys.argv))

db = "/home/pi/ChytryVodomer/db/chytryvodomer.db"
reader = sqlite3.connect(db, isolation_level=None, check_same_thread=False)
reader.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))
reader.execute('''PRAGMA journal_mode=WAL;''')
reader.commit()

cur = reader.cursor()
cur.execute('''SELECT * FROM nastaveni_email ORDER BY Ulozeno DESC LIMIT 1''')
nasEmail = cur.fetchone()
cur.close()

cur = reader.cursor()
cur.execute('''SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1''')
globalNastaveni = cur.fetchone()
cur.close()

try:
	s = smtplib.SMTP_SSL(host=nasEmail["Server"], port=int(nasEmail["Port"]))
	s.login(nasEmail["Username"], nasEmail["Pass"])
except:
	sys.exit()
	
def send(body):
	msg = MIMEMultipart()
	msg['From']= nasEmail["FromEmail"]
	msg['To']= globalNastaveni["Email"]
	msg['Subject']= "WATER GUARD - Hlášení"
	msg.attach(MIMEText(body, 'plain'))
	s.send_message(msg)
	del msg

def main():
	canSend = False
	if globalNastaveni is not None:
		if globalNastaveni["AutoSendMail"] == 1:
			canSend = True
	if canSend:
		send(sys.argv[1])
	
if __name__ == '__main__':
	main()