#!/usr/bin/python3
import os,sys
import sqlite3
import json
from passlib.context import CryptContext
from crontab import CronTab

pwd_context = CryptContext(
	schemes=["pbkdf2_sha256"],
	default="pbkdf2_sha256",
	pbkdf2_sha256__default_rounds=30000)


def enc_pwd(password):
	return pwd_context.hash(password)

def main():
	infoData = None
	wsKey = None
	with open('/home/pi/info/info.json','r') as f:
		infoData = json.load(f)
	
	with open('/home/pi/info/wskey','r') as f:
		wsKey = f.read()
	
	ventilDef = infoData['VentilDefStav']
	
	if len(sys.argv) > 1:
		wsKey = sys.argv[1]
		with open('/home/pi/info/wskey','w') as f:
			f.write(wsKey)
	
	if len(sys.argv) > 2:
		ventilDef = sys.argv[2]

	if ventilDef == None:
		ventilDef = 0

	conn = sqlite3.connect("/home/pi/ChytryVodomer/db/chytryvodomer.db")
	print("DB Connected")

	conn.row_factory = lambda c, r: dict(zip([col[0] for col in c.description], r))
	print("Creating tables")

	conn.execute('''PRAGMA journal_mode=OFF''')
	conn.commit()

	conn.execute('''CREATE TABLE IF NOT EXISTS nastaveni_global (ID INTEGER PRIMARY KEY, LimitLitry INTEGER NOT NULL DEFAULT 0, LimitJednotka INTEGER DEFAULT 1, PocatecniStavLitry INTEGER NOT NULL DEFAULT 0, Email TEXT, AutoSendMail INTEGER NOT NULL DEFAULT 0, Tel TEXT, HesloTel TEXT, WSServer TEXT, WSKey TEXT, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS ventil_nastaveni (ID INTEGER PRIMARY KEY, ReleMod TEXT DEFAULT 'NO', VentilMod TEXT DEFAULT 'NC', VychoziStav INTEGER NOT NULL DEFAULT 0, AutoMod INTEGER NOT NULL DEFAULT 1, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS vodomer_nastaveni (ID INTEGER PRIMARY KEY, PulsLitr INTEGER NOT NULL DEFAULT 1, PrvniMereni DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')), Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS nastaveni_email (ID INTEGER PRIMARY KEY,  Server TEXT, Port INTEGER NOT NULL DEFAULT 0, Security TEXT, Username TEXT, Pass TEXT, FromEmail TEXT, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS uzivatele (ID INTEGER PRIMARY KEY, Login TEXT, Heslo TEXT, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS ventil_data (ID INTEGER PRIMARY KEY, StavVentil INTEGER, AutoAkce INTEGER, Force INTEGER DEFAULT 0, TsTime TIMESTAMP DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW', 'localtime')), Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS vodomer_data (ID INTEGER PRIMARY KEY, PocetLitry INTEGER NOT NULL DEFAULT 0, Prutok INTEGER NOT NULL DEFAULT 0, StavVentil INTEGER NOT NULL DEFAULT 0, CelkemLitry INTEGER NOT NULL DEFAULT 0, TsTime TIMESTAMP DEFAULT (STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW', 'localtime')), Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS vodomer_pulsy (ID INTEGER PRIMARY KEY, Pocet INTEGER, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')
	
	conn.execute('''CREATE TABLE IF NOT EXISTS vodomer_temp (AktPocet INTEGER, AktPrutok REAL, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''CREATE TABLE IF NOT EXISTS events (ID INTEGER PRIMARY KEY, Title TEXT, Start TEXT, End TEXT, AllDay TEXT, Repeat TEXT DEFAULT 'none', StartAkce INTEGER, KonecAkce INTEGER, StavVentil INTEGER DEFAULT 0, LimitJednotka INTEGER DEFAULT 0, LimitVal INTEGER, Ulozeno DATETIME NOT NULL DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))''')

	conn.execute('''DELETE FROM nastaveni_global''')
	conn.execute('''DELETE FROM ventil_nastaveni''')
	conn.execute('''DELETE FROM vodomer_nastaveni''')
	conn.execute('''DELETE FROM nastaveni_email''')
	conn.execute('''DELETE FROM uzivatele''')
	conn.execute('''DELETE FROM vodomer_pulsy''')
	conn.execute('''DELETE FROM ventil_data''')
	conn.execute('''DELETE FROM vodomer_data''')
	conn.execute('''DELETE FROM events''')

	conn.execute('''INSERT INTO nastaveni_global (LimitLitry, LimitJednotka, WSServer, WSKey) VALUES (0, 0, 'device.fengoo.cz/ws/v2', ?)''', (wsKey,))
	conn.execute('''INSERT INTO ventil_nastaveni (VychoziStav) VALUES (?)''', (ventilDef,))
	conn.execute('''INSERT INTO vodomer_nastaveni (PulsLitr) VALUES (1)''')
	conn.execute('''INSERT INTO nastaveni_email (Server, Port, Security, Username, Pass, FromEmail) VALUES ('', 465, 'SSL', '', '', '')''')
	conn.execute('''INSERT INTO uzivatele (Login, Heslo) VALUES ('Admin', ?)''',(enc_pwd('1234'),))
	conn.execute('''INSERT INTO vodomer_temp (AktPocet, AktPrutok) VALUES (0,0)''')
	#conn.execute('''INSERT INTO vodomer_data (PocetLitry) VALUES (0)''')
	#conn.execute('''INSERT INTO ventil_data (StavVentil) VALUES (0)''')
	
	cur1 = conn.cursor()
	cur2 = conn.cursor()
	cur3 = conn.cursor()
	cur4 = conn.cursor()
	cur5 = conn.cursor()
	cur6 = conn.cursor()
	cur7 = conn.cursor()
	cur8 = conn.cursor()
	cur9 = conn.cursor()

	cur1.execute('''SELECT * FROM nastaveni_global''')
	cur2.execute('''SELECT * FROM ventil_nastaveni''')
	cur3.execute('''SELECT * FROM vodomer_nastaveni''')
	cur4.execute('''SELECT * FROM nastaveni_email''')
	cur5.execute('''SELECT * FROM uzivatele''')
	cur6.execute('''SELECT * FROM vodomer_pulsy''')
	cur7.execute('''SELECT * FROM ventil_data''')
	cur8.execute('''SELECT * FROM vodomer_data''')
	cur9.execute('''SELECT * FROM events''')

	rows1 = cur1.fetchall()
	rows2 = cur2.fetchall()
	rows3 = cur3.fetchall()
	rows4 = cur4.fetchall()
	rows5 = cur5.fetchall()
	rows6 = cur6.fetchall()
	rows7 = cur7.fetchall()
	rows8 = cur8.fetchall()
	rows9 = cur9.fetchall()

	print("TABLES DATA nastaveni_global")
	print(rows1)

	print("TABLES DATA ventil_nastaveni")
	print(rows2)

	print("TABLES DATA vodomer_nastaveni")
	print(rows3)

	print("TABLES DATA nastaveni_email")
	print(rows4)

	print("TABLES DATA uzivatele")
	print(rows5)

	print("TABLES DATA vodomer_pulsy")
	print(rows6)

	print("TABLES DATA ventil_data")
	print(rows7)

	print("TABLES DATA vodomer_data")
	print(rows8)
	
	print("TABLES DATA events")
	print(rows9)

	conn.commit()
	conn.close()
	
	cron = CronTab(user='root')
	cron.remove_all()
	cron.write()
	
	job = cron.new(command="sudo hwclock -s")
	job.every_reboot()
	cron.write()
	
	job0 = cron.new(command="sudo python3 /home/pi/info/updatesCheck.py")
	job0.setall("0 2 * * 0-6")
	cron.write()
	
	job1 = cron.new(command="sudo python3 /home/pi/ChytryVodomer/ventil.py")
	job1.setall("0 3 * * 0-6")
	cron.write()

	job2 = cron.new(command="sudo cp -rf /home/pi/ChytryVodomer/db/* /home/pi/backup/ChytryVodomer/db/")
	job2.setall("30 2 * * 0-6")
	cron.write()

if __name__ == "__main__":
	main()
