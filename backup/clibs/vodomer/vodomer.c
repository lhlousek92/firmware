#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wiringPi.h>
#include <time.h>
#include <sys/types.h>
#include "db.h"

#define NO_LIMIT 0
#define LIMIT_L 1
#define LIMIT_H 2
#define LIMIT_M 3
#define LIMIT_FLOW_H 4
#define LIMIT_FLOW_M 5

#define VODOMER_PIN 3
#define VENTIL_PIN 0

static volatile unsigned int vodPulses = 0;

void sendMail(int limit, int limitVal)
{
	char cmd[1024] = "";
	
	if(limit == LIMIT_L)
	{
		sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"%s %d L\"", "Ventil uzavřen. Byl dosažen limit", limitVal);
	} 
	else if(limit == LIMIT_H)
	{
		sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"%s %d hod.\"", "Ventil uzavřen. Byl dosažen časový limit", limitVal);
	}
	else if(limit == LIMIT_M)
	{
		sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"%s %d min.\"", "Ventil uzavřen. Byl dosažen časový limit", limitVal);
	}
	else if(limit == LIMIT_FLOW_H)
	{
		sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"%s %d L/hod.\"", "Ventil uzavřen. Byl překročen průtok", limitVal);
	} else if(limit == LIMIT_FLOW_M)
	{
		sprintf(cmd, "sudo python3 /home/pi/ChytryVodomer/mail.py \"%s %d L/min.\"", "Ventil uzavřen. Byl překročen průtok", limitVal);
	} else {
		
	}
	//printf("%s \n", cmd);
	system(cmd);
}

unsigned long long getNow()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	unsigned long long now = (unsigned long long)ts.tv_sec * 1000000000U + (unsigned long long)ts.tv_nsec;
	return now;
}

void vodomerISR (void)
{
	vodPulses++;
}

int main (void)
{	
	struct ActualData actData = {0,0,0,0};
	struct DevConfig devConfig;
	int prvniPuls = 0;
	int prutokPreteceno = 0;
	int aktPulsPocet = 0;
	int limitVal = 1;
	int limitType = 0;
	int aktPocetLitry = 0;
	int pocetLitryCelkem = 0;
	double min_tbp = 0;
	double prutok = 0;
	unsigned long long startT=0;
	unsigned long long startE=0;	
	int started = 0;
	int latstId = 0;
	time_t startTime;
	time_t endTime;
	
	time_t startPulsTime;
	time_t endPulsTime;
	
	double seconds = 0;
	double elapsedTime = 0;
	
	double pulsSeconds = 0;
	
	wiringPiSetup();
	pinMode(VODOMER_PIN, INPUT);
	pinMode(VENTIL_PIN, OUTPUT);
	
	pullUpDnControl(VODOMER_PIN, PUD_UP);  
	  
	if (wiringPiISR(VODOMER_PIN, INT_EDGE_FALLING, &vodomerISR) < 0)
	{
		printf("%s \n", "Unable to setup ISR (vodomerISR)");
		return 1;
	}
	
	actData = getActualData();
	devConfig = getDevConfig();
	
	limitVal = devConfig.limit;
	limitType = devConfig.limitType;
	
	aktPocetLitry = actData.pocetLitry;
	pocetLitryCelkem = actData.pocetLitryCelkem;
	
	startT = getNow();
	int stav = 0;
	time(&endTime);
	time(&startTime);
	time(&startPulsTime);
	time(&endPulsTime);
	
	
	stav = getStavVentil();
	if(stav < 0)
	{
		stav = devConfig.vychoziStav;
	}
	setStavVentil(stav,1);
	
	int lastIo = stav;
	
	while(1)
	{			
		
		time(&endTime);
		time(&endPulsTime);
		devConfig = getDevConfig();
		limitVal = devConfig.limit;
		limitType = devConfig.limitType;
		
		stav = digitalRead(VENTIL_PIN);
		if(lastIo != stav)
		{
			actData = getActualData();
			aktPocetLitry = actData.pocetLitry;
			lastIo = stav;
		}
		
		if(started == 0 && stav == 1 && (limitType == LIMIT_H || limitType == LIMIT_M))
		{
			time(&startTime);
			started = 1;
		}
		
		if(vodPulses != aktPulsPocet)
		{	
			actData = getActualData();
			aktPocetLitry = actData.pocetLitry;
			
			startE = getNow();
			double tbp = (double)(startE - startT) / 1000000000.0;
			startT = getNow();
			min_tbp = (double)60.0/limitVal;
				
			if(limitType == LIMIT_FLOW_H)
				min_tbp = (double)3600.0/limitVal;
			
			if(min_tbp > tbp && prvniPuls == 1)
				prutokPreteceno = 1;
			else
				prutokPreteceno = 0;
				
			if(prvniPuls == 0)
				prvniPuls = 1;
				
			prutok = (double)(60 / tbp);
			time(&startPulsTime);
			
			aktPulsPocet = vodPulses;
			if(aktPulsPocet >= devConfig.pulsLitr)
			{
				aktPocetLitry++;
				pocetLitryCelkem++;				
				latstId = insertVodData(aktPocetLitry, pocetLitryCelkem, prutok, stav);
				aktPulsPocet = 0;
				vodPulses = 0;
				
				printf("%s %d %d %d %d %f %f %d\n", "PULS", aktPocetLitry, pocetLitryCelkem, limitType, limitVal, min_tbp, tbp, prutokPreteceno);				
			}
			
			if(prutokPreteceno == 1 && (limitType == LIMIT_FLOW_H || limitType == LIMIT_FLOW_M))
			{				
				setStavVentil(0,1);
				prutokPreteceno = 0;
				sendMail(limitType, limitVal);
			}
			
			if(limitVal <= aktPocetLitry && limitType == LIMIT_L)
			{
				setStavVentil(0,1);				
				sendMail(limitType, limitVal);
			}									
		}
		
		if(prutok > 0 && latstId != 0)
		{
			pulsSeconds = difftime(endPulsTime, startPulsTime);
			if(pulsSeconds>=10)
			{
				prutok = 0;
				pulsSeconds = 0;
				resetPrutok(latstId);
				time(&startPulsTime);
			}
		} else 
			time(&startPulsTime);
		
		if((limitType == LIMIT_H || limitType == LIMIT_M) && started == 1)
		{
			seconds = difftime(endTime, startTime);
			seconds = seconds+1;
			if(seconds>0)
				elapsedTime = seconds / 60;
			
			if(limitType == LIMIT_H)
				elapsedTime = seconds / 3600;
				
			if(elapsedTime >= limitVal)
			{
				setStavVentil(0,1);
				started = 0;
				elapsedTime = 0;
				seconds = 0;
				
				sendMail(limitType, limitVal);
			}
			
			printf("Elapsed %.1f \n", elapsedTime);
		}
		delay(300);
	}
	  
	return 0;
}