#include "db.h"

char* str_replace(char* string, const char* substr, const char* replacement)
{
	char* tok = NULL;
	char* newstr = NULL;
	char* oldstr = NULL;
	int   oldstr_len = 0;
	int   substr_len = 0;
	int   replacement_len = 0;

	newstr = strdup(string);
	substr_len = strlen(substr);
	replacement_len = strlen(replacement);

	if (substr == NULL || replacement == NULL) {
		return newstr;
	}

	while ((tok = strstr(newstr, substr))) {
		oldstr = newstr;
		oldstr_len = strlen(oldstr);
		newstr = (char*)malloc(sizeof(char) * (oldstr_len - substr_len + replacement_len + 1));

		if (newstr == NULL) {
			return NULL;
		}

		memcpy(newstr, oldstr, tok - oldstr);
		memcpy(newstr + (tok - oldstr), replacement, replacement_len);
		memcpy(newstr + (tok - oldstr) + replacement_len, tok + substr_len, oldstr_len - substr_len - (tok - oldstr));
		memset(newstr + oldstr_len - substr_len + replacement_len, 0, 1);
	}
	return newstr;
}

void clearDbResult(sqlite3_stmt *res, sqlite3 *db)
{
	sqlite3_finalize(res);
	sqlite3_close(db);
}

void printSQLVersion()
{	
	const char *sql = "SELECT SQLITE_VERSION()";
	
	sqlite3_stmt *res;
	sqlite3 *db;
	int rc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
	  
	if (rc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_step(res);
	if (rc == SQLITE_ROW) {
		printf("DB version %s\n", sqlite3_column_text(res, 0));
	}
    clearDbResult(res,db);
}

struct DevConfig getDevConfig()
{
	struct DevConfig cfg;
	const char *sqlGlobNas = "SELECT * FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1";
	const char *sqlVenNas = "SELECT * FROM ventil_nastaveni ORDER BY Ulozeno DESC LIMIT 1";
	const char *sqlVodNas = "SELECT * FROM vodomer_nastaveni ORDER BY Ulozeno DESC LIMIT 1";
	
	sqlite3_stmt *res, *res_ven, *res_vod;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
		
	int rc = sqlite3_prepare_v2(db, sqlGlobNas, -1, &res, 0);
	int rc1 = sqlite3_prepare_v2(db, sqlVenNas, -1, &res_ven, 0);
	int rc2 = sqlite3_prepare_v2(db, sqlVodNas, -1, &res_vod, 0);
	
	if (rc != SQLITE_OK && rc1 != SQLITE_OK && rc2 != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_step(res);
	rc1 = sqlite3_step(res_ven);
	rc2 = sqlite3_step(res_vod);
	
	if (rc == SQLITE_ROW) {
		cfg.limit = (int)sqlite3_column_int(res, 1);
		cfg.limitType = (int)sqlite3_column_int(res, 2);
		cfg.autoSendMail = (int)sqlite3_column_int(res,5);
	} else {
		delay(50);
		rc = sqlite3_step(res);
		if (rc == SQLITE_ROW) {
			cfg.limit = (int)sqlite3_column_int(res, 1);
			cfg.limitType = (int)sqlite3_column_int(res, 2);
			cfg.autoSendMail = (int)sqlite3_column_int(res,5);
		} 
	}
	
	if (rc1 == SQLITE_ROW) {
		cfg.vychoziStav = (int)sqlite3_column_int(res_ven, 3);
		cfg.autoMod = (int)sqlite3_column_int(res_ven, 4);
	} else {
		delay(50);
		rc1 = sqlite3_step(res_ven);
		if (rc1 == SQLITE_ROW) {
			cfg.vychoziStav = (int)sqlite3_column_int(res_ven, 3);
			cfg.autoMod = (int)sqlite3_column_int(res_ven, 4);
		}
	}
	
	if (rc2 == SQLITE_ROW) {
		cfg.pulsLitr = (int)sqlite3_column_int(res_vod, 1);
	} else {
		delay(50);
		rc2 = sqlite3_step(res_vod);
		if (rc2 == SQLITE_ROW) {
			cfg.pulsLitr = (int)sqlite3_column_int(res_vod, 1);
		}
	}
		
	sqlite3_finalize(res);
	sqlite3_finalize(res_ven);
    sqlite3_finalize(res_vod);
	sqlite3_close(db);
	return cfg;
}

struct EmailConfig getEmailConfig()
{
	struct EmailConfig cfg;
	const char *sqlGlobNas = "SELECT Email FROM nastaveni_global ORDER BY Ulozeno DESC LIMIT 1";
	const char *sqlEmailNas = "SELECT * FROM nastaveni_email ORDER BY Ulozeno DESC LIMIT 1";
	
	sqlite3_stmt *res, *res_email;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
		
	int rc = sqlite3_prepare_v2(db, sqlGlobNas, -1, &res, 0);
	int rc1 = sqlite3_prepare_v2(db, sqlEmailNas, -1, &res_email, 0);
	
	if (rc != SQLITE_OK && rc1 != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_step(res);
	rc1 = sqlite3_step(res_email);
	
	if (rc == SQLITE_ROW) {
		cfg.receiver = (char *)sqlite3_column_text(res, 0);
	}
	
	if (rc1 == SQLITE_ROW) {
		cfg.SMTP_Host = (char *)sqlite3_column_text(res_email, 1);
		cfg.SMTP_Port = (int)sqlite3_column_int(res_email, 2);
		cfg.security = (char *)sqlite3_column_text(res_email, 3);
		cfg.user = (char *)sqlite3_column_text(res_email, 4);
		cfg.password = (char *)sqlite3_column_text(res_email, 5);
		cfg.sender = (char *)sqlite3_column_text(res_email, 6);
	}
		
	sqlite3_finalize(res);
	sqlite3_finalize(res_email);
	sqlite3_close(db);
	
	return cfg;
}

struct ActualData getActualData()
{
	struct ActualData actData = { 0, 0, 0, 0 };
		
	const char *sqlVodData = "SELECT PocetLitry, CelkemLitry FROM vodomer_data ORDER BY TsTime DESC LIMIT 1";
	const char *sqlVenData = "SELECT StavVentil, AutoAkce FROM ventil_data ORDER BY TsTime DESC LIMIT 1";
	
	sqlite3_stmt *resVod, *resVen;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
		
	int rc = sqlite3_prepare_v2(db, sqlVodData, -1, &resVod, 0);
	int rc1 = sqlite3_prepare_v2(db, sqlVenData, -1, &resVen, 0);
	
	if (rc != SQLITE_OK && rc1 != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
	}
	
	rc = sqlite3_step(resVod);
	rc1 = sqlite3_step(resVen);
	
	if (rc == SQLITE_ROW) {
		actData.pocetLitry = (int)sqlite3_column_int(resVod, 0);
		actData.pocetLitryCelkem = (int)sqlite3_column_int(resVod, 1);
	} else {
		delay(50);
		rc = sqlite3_step(resVod);
		if (rc == SQLITE_ROW) {
			actData.pocetLitry = (int)sqlite3_column_int(resVod, 0);
			actData.pocetLitryCelkem = (int)sqlite3_column_int(resVod, 1);
		}
	}
	
	if (rc1 == SQLITE_ROW) {
		actData.stavVentil = (int)sqlite3_column_int(resVen, 0);
		actData.autoMod = (int)sqlite3_column_int(resVen, 1);
	} else {
		actData.stavVentil = -1;
		actData.autoMod = 1;
		delay(50);
		rc1 = sqlite3_step(resVen);
		if (rc1 == SQLITE_ROW) {
			actData.stavVentil = (int)sqlite3_column_int(resVen, 0);
			actData.autoMod = (int)sqlite3_column_int(resVen, 1);
		}
	}
	
	sqlite3_finalize(resVod);
	sqlite3_finalize(resVen);
	sqlite3_close(db);
	return actData;
}

int getStavVentil()
{
	int stavVentil = -1;
	const char *sqlVenData = "SELECT StavVentil FROM ventil_data ORDER BY TsTime DESC LIMIT 1";
	
	sqlite3_stmt *res;
	sqlite3 *db;

	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sqlVenData, -1, &res, 0);
	rc = sqlite3_step(res);
	if (rc == SQLITE_ROW) {
		stavVentil = (int)sqlite3_column_int(res, 0);
	} else {
		delay(50);
		rc = sqlite3_step(res);
		if (rc == SQLITE_ROW) {
			stavVentil = (int)sqlite3_column_int(res, 0);
		}
	}
	
	clearDbResult(res, db);
	return stavVentil;
}

void setStavVentil(int stav, int autoMod)
{
	/*struct EventData evt = {};
	evt = findEvent();
	/*/
	
	const char *sql = "INSERT INTO ventil_data (StavVentil, AutoAkce) VALUES (?,?)";
	sqlite3_stmt *res;
	sqlite3 *db;
		
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
	
		sqlite3_bind_int(res,1,stav);
		sqlite3_bind_int(res,2,autoMod);
		rc = sqlite3_step(res);
		
		if (rc != SQLITE_DONE)
		{
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE)
				printf("execution failed: %s", sqlite3_errmsg(db));
		}
		clearDbResult(res, db);
	}
}

int insertVodData(int pocetL, int celkemL, int prutok, int stavVentil)
{
	int id = 0;
	
	const char *sql = "INSERT INTO vodomer_data (PocetLitry, Prutok, StavVentil, CelkemLitry) VALUES (?,?,?,?)";
	sqlite3_stmt *res;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
		sqlite3_bind_int(res,1,pocetL);
		sqlite3_bind_int(res,2,prutok);
		sqlite3_bind_int(res,3,stavVentil);
		sqlite3_bind_int(res,4,celkemL);
	
		rc = sqlite3_step(res);
    
		if (rc != SQLITE_DONE) { 
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE)
				printf("execution failed: %s\n", sqlite3_errmsg(db));
		}
		
		id = sqlite3_last_insert_rowid(db);
		
		clearDbResult(res, db);
	}
	return id;
}

/*
struct EventData {
	int id;
	char *start;
	char *end;
	char *actionStart;
	char *actionEnd;
	int limitType;
	int limitVal;
};


struct EventData findEvent()
{
	struct EventData events[1000];
	struct EventData event = {};
	event.valid = 0;
	
	const char *sql = "SELECT * FROM events WHERE StavVentil = 0 ORDER BY Ulozeno DESC";
	sqlite3_stmt *res;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare update statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {	
		rc = sqlite3_step(res);
		int rowIdx = 0;
		
		time_t actDate;
		time(&actDate);
		localtime(&actDate);
		
		int finded = 0;
		while(rc != SQLITE_DONE)
		{
			if(rowIdx >= 999)
				rowIdx = 999;
			
			char *start = (char *)sqlite3_column_text(res,2);
			char *end = (char *)sqlite3_column_text(res,3);
			
			char *evtStartStr = str_replace(start, "T", " ");
			evtStartStr = str_replace(evtStartStr, ":00Z", "");
			
			char *evtEndStr = str_replace(end, "T", " ");
			evtEndStr = str_replace(evtEndStr, ":00Z", "");
			
			time_t tStart;
			time_t tEnd;
			struct tm tmStart, tmEnd;
			
			strptime(evtStartStr, "%Y-%m-%d %H:%M", &tmStart);
			strptime(evtEndStr, "%Y-%m-%d %H:%M", &tmEnd);
			
			tStart = mktime(&tmStart);
			tEnd = mktime(&tmEnd);
			
			//printf("Now time: %s\n",ctime(&actDate));
			
			double diffStart = difftime(actDate, tStart);
			double diffEnd = difftime(tEnd, actDate);
			//printf("Event start : %f Event end : %f\n", diffStart, diffEnd);
			
			if(diffStart >= 0 && diffEnd > 0)
			{
				events[rowIdx].id = (int)sqlite3_column_int(res, 0);
				events[rowIdx].title = (char *)sqlite3_column_text(res,1);
				events[rowIdx].start = (char *)sqlite3_column_text(res,2);
				events[rowIdx].end = (char *)sqlite3_column_text(res,3);
				events[rowIdx].actionStart = (int)sqlite3_column_int(res,5);
				events[rowIdx].actionEnd = (int)sqlite3_column_int(res,6);
				events[rowIdx].limitType = (int)sqlite3_column_int(res, 8);
				events[rowIdx].limitVal = (int)sqlite3_column_int(res, 9);
				events[rowIdx].stavVentil = (int)sqlite3_column_int(res, 7);
				events[rowIdx].t_Start = tStart;
				events[rowIdx].t_End = tEnd;
				
				events[rowIdx].valid = 1;
				
				event = events[rowIdx];
				
				finded = 1;				
			}
			
			if(finded == 0)
			{
				rowIdx++;
				rc = sqlite3_step(res);
			} else
				break;
		}
				
		clearDbResult(res, db);
	}
	
	return event;
}
*/

void setEventVentil(struct EventData evt)
{
	const char *sql = "UPDATE events SET StavVentil = ? WHERE ID = ?";
	sqlite3_stmt *res;
	sqlite3 *db;
		
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare insert statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
			
		sqlite3_bind_int(res,1,1);
		sqlite3_bind_int(res,2,evt.id);
		rc = sqlite3_step(res);
		
		if (rc != SQLITE_DONE)
		{
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE)
				printf("execution failed: %s", sqlite3_errmsg(db));
		}	
		clearDbResult(res, db);
	}
}

void resetPrutok(int id)
{
	const char *sql = "UPDATE vodomer_data SET Prutok = ? WHERE ID = ?";
	sqlite3_stmt *res;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare update statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
		sqlite3_bind_int(res,1,0);
		sqlite3_bind_int(res,2,id);
	
		rc = sqlite3_step(res);
    
		if (rc != SQLITE_DONE) { 
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE)
				printf("execution failed: %s\n", sqlite3_errmsg(db));
		}
	
		clearDbResult(res, db);
	}
}

void updateLimit(struct EventData evt)
{
	const char *sql = "UPDATE nastaveni_global SET LimitLitry = ?, LimitJednotka = ? WHERE ID = ?";
	const char *sqlS = "SELECT * FROM nastaveni_global ORDER BY Ulozeno LIMIT 1";
	int id;
	sqlite3_stmt *res;
	sqlite3_stmt *resId;
	sqlite3 *db;
	
	int rcc = sqlite3_open("/home/pi/ChytryVodomer/db/chytryvodomer.db", &db);
	
	if (rcc != SQLITE_OK)
	{
		sqlite3_close(db);
		fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
	}
	
	int rcId = sqlite3_prepare_v2(db, sqlS, -1, &resId, NULL);
	if(rcId != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare select statment %s (%i): %s\n", sqlS, rcId, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
		rcId = sqlite3_step(resId);
		if (rcId != SQLITE_DONE)
		{
			delay(50);
			rcId = sqlite3_step(resId);
			if (rcId != SQLITE_DONE)
				printf("execution failed: %s\n", sqlite3_errmsg(db));
		}
		id = (int)sqlite3_column_int(resId, 0);		
		sqlite3_finalize(resId);
	}
	
	int rc = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	
	if(rc != SQLITE_OK)
	{
		fprintf(stderr, "Can't prepare update statment %s (%i): %s\n", sql, rc, sqlite3_errmsg(db));
		sqlite3_close(db);
	} else {
		sqlite3_bind_int(res,1,evt.limitVal);
		sqlite3_bind_int(res,2,evt.limitType);
		sqlite3_bind_int(res,3,id);
	
		rc = sqlite3_step(res);
    
		if (rc != SQLITE_DONE) { 
			delay(50);
			rc = sqlite3_step(res);
			if (rc != SQLITE_DONE)
				printf("execution failed: %s\n", sqlite3_errmsg(db));
		}
		
		clearDbResult(res, db);
	}
}