#!/usr/bin/python3
"""ventil.py: program pro údržbu ventilu"""
__author__      = "Ladislav Hlousek"
__copyright__   = "Copyright 2018, Ladislav Hlousek"
__version__     = "1.0.0"
__maintainer__  = "Ladislav Hlousek"
__email__       = "ladishlousek@gmail.com"
__status__      = "Production"

import os,sys,signal
import RPi.GPIO as GPIO
from time import sleep

ventil_pin = 17
lastIoState = 0

os.system("sudo systemctl stop vodomer")
os.system("sudo systemctl stop ventil")
os.system("sudo systemctl stop btn")
os.system("sudo systemctl stop webapp")
os.system("sudo systemctl stop wsagent")
sleep(1)

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(ventil_pin, GPIO.OUT)

lastIoState = GPIO.input(ventil_pin)
print(lastIoState)
if lastIoState == 1:
	for i in range(1,5):
		GPIO.output(ventil_pin, GPIO.LOW)
		print(0)
		sleep(2)
		GPIO.output(ventil_pin, GPIO.HIGH)
		print(1)
		sleep(2)
else:
	for i in range(1,5):
		GPIO.output(ventil_pin, GPIO.HIGH)
		print(1)
		sleep(2)
		GPIO.output(ventil_pin, GPIO.LOW)
		print(0)
		sleep(2)

os.system("sudo systemctl start vodomer")
os.system("sudo systemctl start ventil")
os.system("sudo systemctl start btn")
os.system("sudo systemctl start webapp")
os.system("sudo systemctl start wsagent")